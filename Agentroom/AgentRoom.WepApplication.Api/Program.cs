using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Agentroom.Common.Settings;
using Agentroom.DataAccessLayer.Context;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Serilog;
using Serilog.Core;

namespace AgentRoom.WepApplication.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost webHost = CreateHostBuilder(args).Build();

            LogAndRun(webHost);
        }

        public static IWebHostBuilder CreateHostBuilder(string[] args) =>
            WebHost
                .CreateDefaultBuilder(args)
                .ConfigureAppConfiguration(ConfigConfiguration)
                .ConfigureLogging((_, logging) => logging.ClearProviders())
                .UseStartup<Startup>()
                .UseSerilog();

        private static void LogAndRun(IWebHost webHost)
        {
            Log.Logger = BuildLogger(webHost);

            try
            {
                ApplyMigrations(webHost);
                webHost.Run();
            }
            catch(Exception ex)
            {
                Log.Fatal(ex, "Application terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static void ConfigConfiguration(WebHostBuilderContext builderContext, IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
        }

        private static Logger BuildLogger(IWebHost webHost)
        {
            return new LoggerConfiguration().ReadFrom
                .Configuration(webHost.Services.GetRequiredService<IConfiguration>())
                .CreateLogger();
        }

        private static void ApplyMigrations(IWebHost webHost)
        {
            try
            {
                using (var scope = webHost.Services.CreateScope())
                {
                    var settings = scope.ServiceProvider.GetRequiredService<IOptionsMonitor<DbConnectionSettings>>();
                    if (settings.CurrentValue.ShouldAutomaticallyApplyMigrations)
                    {
                        var context = scope.ServiceProvider.GetRequiredService<AgentroomContext>();
                        context.Database.Migrate();
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Migrations application error");
                throw;
            }
        }
    }
}
