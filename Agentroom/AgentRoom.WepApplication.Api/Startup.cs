
using System;

using Agentroom.Common.Settings;
using Agentroom.DataAccessLayer;
using Agentroom.DataAccessLayer.Configuration;

using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace AgentRoom.WepApplication.Api
{
    public class Startup
    {
        private readonly IConfiguration configuration;
        private readonly IWebHostEnvironment environment;

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            this.configuration = configuration;
            this.environment = environment;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddUnitOfWork();
            services.AddSingleton(configuration);
            //services.AddMemoryCache();
            //services.AddSession();
            services.AddDbConnectionSettingsConfigurationSection(configuration);
            services.AddMyContext(isDevelopment: environment.IsDevelopment());

            AddFluentMigrator(services);
        }

        private void AddFluentMigrator(IServiceCollection services)
        {
            services.AddFluentMigratorCore()
                            .ConfigureRunner(builder => builder
                            .AddPostgres()
                            .WithGlobalConnectionString(provider =>
                                provider.GetService<IOptionsMonitor<DbConnectionSettings>>().CurrentValue.ConnectionString)
                            .WithGlobalCommandTimeout(TimeSpan.FromMinutes(10))
                            .ScanIn(typeof(BaseMigration).Assembly).For.Migrations());

            services.Configure<RunnerOptions>(runnerOptions =>
            {
                if (!string.IsNullOrEmpty(environment.EnvironmentName))
                {
                    runnerOptions.Tags = new[] { environment.EnvironmentName };
                }
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
