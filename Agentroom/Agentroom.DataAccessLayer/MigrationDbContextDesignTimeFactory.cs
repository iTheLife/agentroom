﻿using System.Reflection;

using Agentroom.DataAccessLayer.Context;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Agentroom.DataAccessLayer
{
    internal class MigrationDbContextDesignTimeFactory : IDesignTimeDbContextFactory<AgentroomContext>
    {
        public AgentroomContext CreateDbContext(string[] args)
        {
            var configuration = ConfigurationFactory.Create(args);
            var builder = new DbContextOptionsBuilder<AgentroomContext>();
            builder.UseNpgsql(configuration.GetConnectionString("DefaultConnection"),
                options => options.MigrationsAssembly(Assembly.GetExecutingAssembly().GetName().Name));

            return new AgentroomContext(builder.Options, false);
        }
    }
}
