﻿using System;

using Agentroom.Common.Interfaces;
using Agentroom.Common.Settings;
using Agentroom.DataAccessLayer.Context;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Agentroom.DataAccessLayer.Configuration
{
    public static class AgentRoomDALExtension
    {
        public static void AddUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }

        public static void AddDbConnectionSettingsConfigurationSection(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<DbConnectionSettings>(configuration.GetSection(nameof(DbConnectionSettings)));
        }

        public static void AddMyContext(this IServiceCollection services, bool isDevelopment)
        {
            //var agentroomSettings = serviceProvider.GetService<IOptionsMonitor<DbConnectionSettings>>();
            services.AddDbContext<AgentroomContext>((IServiceProvider serviceProvider, DbContextOptionsBuilder optionsBuilder) =>
            {
                var agentroomSettings = serviceProvider.GetService<IOptionsMonitor<DbConnectionSettings>>();
                var loggerFactory = serviceProvider.GetService<ILoggerFactory>();

                optionsBuilder
                    .UseNpgsql(agentroomSettings.CurrentValue.ConnectionString)
                    .UseLoggerFactory(loggerFactory);

                if (isDevelopment)
                {
                    optionsBuilder.EnableSensitiveDataLogging(true);
                }
            });
        }
    }
}
