﻿using System;
using System.Collections.Generic;
using System.Text;

using Agentroom.DataAccessLayer.Model;

namespace Agentroom.Common.Interfaces
{
    public interface IUnitOfWork : IUnitOfWorkBase
    {
        IRepositoryGeneric<Permission> PermissionRepository { get; }
        IRepositoryGeneric<Role> RoleRepository { get; }
        IRepositoryGeneric<User> UserRepository { get; }
    }
}
