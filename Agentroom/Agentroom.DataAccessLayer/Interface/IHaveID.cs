﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Agentroom.DataAccessLayer.Interface
{
    interface IHaveID
    {
        Guid Id { get; set; }
    }
}
