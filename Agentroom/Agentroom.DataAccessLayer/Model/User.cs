﻿using Agentroom.DataAccessLayer.Base;
using Agentroom.DataAccessLayer.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agentroom.DataAccessLayer.Model
{
    public class User : EntityBase
    {
        public string Login { get; set; }
        public bool IsActive { get; set; }
        public virtual IEnumerable<Role> Roles { get; set; }

    }
}
