﻿using System.Collections.Generic;

using Agentroom.DataAccessLayer.Base;

namespace Agentroom.DataAccessLayer.Model
{
    public class Permission : EntityBase
    {
        public string PermissionName { get; set; }
        public virtual IEnumerable<Role> Roles { get; set; }
    }
}
