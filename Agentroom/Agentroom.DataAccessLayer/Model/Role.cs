﻿using System;
using System.Collections.Generic;

using Agentroom.DataAccessLayer.Base;

namespace Agentroom.DataAccessLayer.Model
{
    public class Role : EntityBase
    {
        public string RoleName { get; set; }
        public virtual IEnumerable<User> Users { get; set; }
        public virtual IEnumerable<Permission> Permissions { get; set; }

    }
}
