﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using Agentroom.DataAccessLayer.Interface;

namespace Agentroom.DataAccessLayer.Base
{
    public abstract class EntityBase : IHaveID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
    }
}
