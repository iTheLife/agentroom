﻿using System.Reflection;

using Agentroom.Common.Settings;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Agentroom.DataAccessLayer.Context
{
    public class AgentroomContext : EfContext
    {
        public AgentroomContext(DbContextOptions<AgentroomContext> options, bool autoDetectChangesEnabled)
            : base(options)
        {
            ChangeTracker.AutoDetectChangesEnabled = autoDetectChangesEnabled;
        }



        public AgentroomContext(DbContextOptions options, ILogger<AgentroomContext> logger, IOptionsMonitor<DbConnectionSettings> myContextSettings)
            : base(options, logger)
        {
            //if (myContextSettings.CurrentValue.DevEnvCreateDBUsingEF)
            //{
            Database.EnsureCreated();
            //}

            ChangeTracker.AutoDetectChangesEnabled = !myContextSettings.CurrentValue.DisableAutoDetectChanges;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ApplyMappingConfigurationFromAssembly(modelBuilder, Assembly.GetExecutingAssembly());
        }
    }
}
