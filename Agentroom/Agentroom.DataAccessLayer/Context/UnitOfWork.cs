﻿
using Agentroom.Common.Interfaces;
using Agentroom.DataAccessLayer.Model;

using Microsoft.Extensions.Logging;

namespace Agentroom.DataAccessLayer.Context
{
    public class UnitOfWork : EFUnitOfWork, IUnitOfWork
    {
        public UnitOfWork(AgentroomContext dbContext)
            :base(dbContext)
        {
            PermissionRepository = GetRepositoryEntity<Permission>();
            RoleRepository = GetRepositoryEntity<Role>();
            UserRepository = GetRepositoryEntity<User>();
        }

        public UnitOfWork(AgentroomContext dbContext, ILogger<UnitOfWork> logger)
            : base(dbContext, logger)
        {
            PermissionRepository = GetRepositoryEntity<Permission>();
            RoleRepository = GetRepositoryEntity<Role>();
            UserRepository = GetRepositoryEntity<User>();
        }
        public IRepositoryGeneric<Permission> PermissionRepository { get; }

        public IRepositoryGeneric<Role> RoleRepository { get; }

        public IRepositoryGeneric<User> UserRepository { get; }
    }
}
