﻿namespace Agentroom.Common.Settings
{
    public class DbConnectionSettings
    {
        public string ConnectionString { get; set; }

        public bool DevEnvCreateDBUsingEF { get; set; }

        public bool DisableAutoDetectChanges { get; set; }
        public bool ShouldAutomaticallyApplyMigrations { get; set; }
        public bool ShouldApplyDataSeeds { get; set; }
    }
}
