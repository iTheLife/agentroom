﻿using FluentMigrator;

namespace Agentroom.DataAccessLayer
{
    public abstract class BaseMigration : Migration
    {
        public override abstract void Up();

        public override abstract void Down();

    }
}
