﻿using System.IO;

using Microsoft.Extensions.Configuration;

namespace Agentroom.DataAccessLayer
{
    internal static class ConfigurationFactory
    {
        public static IConfiguration Create(params string [] args)
        {
            var configurationBuilder = new ConfigurationBuilder();
            configurationBuilder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .AddEnvironmentVariables()
                .AddCommandLine(args);

            return configurationBuilder.Build();
        }
    }
}
