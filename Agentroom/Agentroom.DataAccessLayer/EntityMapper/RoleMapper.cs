﻿using System;
using System.Collections.Generic;
using System.Text;

using Agentroom.DataAccessLayer.Model;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Agentroom.DataAccessLayer.EntityMapper
{
    internal class RoleMapper : AbstractEntityMapper<Role>
    {
        public override void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("Roles").HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder
                .HasMany(x => x.Users)
                .WithMany(x => x.Roles)
                .UsingEntity(x => x.ToTable("UserRole"));

            builder
                .HasMany(x => x.Permissions)
                .WithMany(x => x.Roles)
                .UsingEntity(x => x.ToTable("RolePermission"));
        }
    }
}
