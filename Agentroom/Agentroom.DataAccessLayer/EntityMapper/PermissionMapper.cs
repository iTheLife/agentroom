﻿using System;
using System.Collections.Generic;
using System.Text;

using Agentroom.DataAccessLayer.Model;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Agentroom.DataAccessLayer.EntityMapper
{
    internal class PermissionMapper : AbstractEntityMapper<Permission>
    {
        public override void Configure(EntityTypeBuilder<Permission> builder)
        {
            builder.ToTable("Permissions").HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder
                .HasMany(x => x.Roles)
                .WithMany(x => x.Permissions)
                .UsingEntity(x => x.ToTable("RolePermission"));
        }
    }
}
