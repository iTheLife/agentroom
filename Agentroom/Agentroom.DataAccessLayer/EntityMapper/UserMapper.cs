﻿using System;
using System.Collections.Generic;
using System.Text;

using Agentroom.DataAccessLayer.Model;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Agentroom.DataAccessLayer.EntityMapper
{
    internal class UserMapper : AbstractEntityMapper<User>
    {
        public override void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User").HasKey(x => x.Id);
            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder
                .HasMany(x => x.Roles)
                .WithMany(x => x.Users)
                .UsingEntity(x => x.ToTable("UserRole"));
        }
    }
}
