﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Agentroom.Common.Interfaces
{
    public interface IUnitOfWorkBase : IDisposable
    {
        void Commit();
        Task<int> CommitAsync(CancellationToken cancellationToken = default);
        void RejectChanges();
        IRepositoryGeneric<TEntity> GetRepositoryEntity<TEntity>() where TEntity : class;

    }
}
