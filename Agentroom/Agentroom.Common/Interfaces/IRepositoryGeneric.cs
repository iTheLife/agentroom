﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Agentroom.Common.Interfaces
{
    /// <summary>
    /// Interface of generic repository
    /// </summary>
    /// <typeparam name="TEntity">Type of entity</typeparam>
    public interface IRepositoryGeneric<TEntity>
    {
        TEntity Create(TEntity entity);
        Task<TEntity> CreateAsync(TEntity entity);
        TEntity Update(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        void Delete(Guid id);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken = default);
        void Delete(TEntity entity);
        Task DeleteAsync(TEntity entity);
        void DeleteAll(Expression<Func<TEntity, bool>> condition = null);
        TEntity FindOne(Expression<Func<TEntity, bool>> filter, bool withDeleted = false, bool trackChanges = false);
        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken = default, bool withDeleted = false, bool trackChanges = false);
        IEnumerable<TEntity> FindAll(Expression<Func<TEntity, bool>> filter, bool withDeleted = false, bool trackChanges = false);
        Task<IEnumerable<TEntity>> FindAllAsync(Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken = default, bool withDeleted = false, bool trackChanges = false);
        IQueryable<TEntity> GetAll(bool withDeleted = false, bool trackChanges = false);
        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter, bool withDeleted = false, bool trackChanges = false);
        long GetCount(Expression<Func<TEntity, bool>> condition = null, bool withDeleted = false);
        Task<long> GetCountAsync(CancellationToken cancellationToken = default, Expression<Func<TEntity, bool>> condition = null, bool withDeleted = false);
        void Detach(TEntity entity);
    }
}
