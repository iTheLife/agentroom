﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Agentroom.Common.Interfaces;
using Agentroom.DataAccessLayer.Context;

using Microsoft.EntityFrameworkCore;

namespace Agentroom.DataAccessLayer.Repository
{
    public class EFGenericRepository<TEntity> : IRepositoryGeneric<TEntity> where TEntity : class
    {
        protected readonly EfContext _dbContext;
        protected readonly DbSet<TEntity> dbSet;

        public EFGenericRepository(EfContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            this.dbSet = _dbContext.Set<TEntity>();
        }

        public virtual TEntity Create(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            return dbSet.Add(entity).Entity;
        }

        public virtual Task<TEntity> CreateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            return Task.FromResult(Create(entity));
        }

        public virtual void Delete(Guid id)
        {
            var entity = dbSet.Find(id);

            if (entity == null)
            {
                throw new KeyNotFoundException();
            }

            Delete(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            AttachIfNot(entity);
            dbSet.Remove(entity);
        }

        public virtual void DeleteAll(System.Linq.Expressions.Expression<Func<TEntity, bool>> condition = null)
        {
            var itemsToDelete = condition != null
                ? FindAll(condition)
                : GetAll();

            if (itemsToDelete.Any())
            {
                foreach (var item in itemsToDelete)
                {
                    Delete(item);
                }
            }
        }

        public virtual async Task DeleteAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var entity = await dbSet.FindAsync(new object[] { id }, cancellationToken);

            if (entity == null)
            {
                throw new KeyNotFoundException();
            }

            Delete(entity);
        }

        public virtual Task DeleteAsync(TEntity entity)
        {
            Delete(entity);

            return Task.CompletedTask;
        }

        public virtual void Detach(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Detached;
        }

        public virtual IEnumerable<TEntity> FindAll(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter, bool withDeleted = false, bool trackChanges = false)
        {
            var query = PrepareQuery(withDeleted, trackChanges);

            return query.Where(filter).ToList();
        }

        public virtual async Task<IEnumerable<TEntity>> FindAllAsync(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken = default, bool withDeleted = false, bool trackChanges = false)
        {
            var query = PrepareQuery(withDeleted, trackChanges);

            return await query.Where(filter).ToListAsync(cancellationToken);
        }

        public virtual TEntity FindOne(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter, bool withDeleted = false, bool trackChanges = false)
        {
            var query = PrepareQuery(withDeleted, trackChanges);

            return query.FirstOrDefault(filter);
        }

        public virtual async Task<TEntity> FindOneAsync(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken = default, bool withDeleted = false, bool trackChanges = false)
        {
            var query = PrepareQuery(withDeleted, trackChanges);

            return await query.FirstOrDefaultAsync(filter, cancellationToken);
        }

        public virtual IQueryable<TEntity> GetAll(bool withDeleted = false, bool trackChanges = false)
        {
            return PrepareQuery(withDeleted, trackChanges);
        }

        public virtual IQueryable<TEntity> GetAll(System.Linq.Expressions.Expression<Func<TEntity, bool>> filter, bool withDeleted = false, bool trackChanges = false)
        {
            var query = PrepareQuery(withDeleted, trackChanges);

            return query.Where(filter);
        }

        public virtual long GetCount(System.Linq.Expressions.Expression<Func<TEntity, bool>> condition = null, bool withDeleted = false)
        {
            var query = PrepareQuery(withDeleted, trackChanges:false);

            return condition != null
                ? query.Count(condition)
                : query.Count();
        }

        public virtual async Task<long> GetCountAsync(CancellationToken cancellationToken = default, System.Linq.Expressions.Expression<Func<TEntity, bool>> condition = null, bool withDeleted = false)
        {
            var query = PrepareQuery(withDeleted, trackChanges: false);

            return condition != null
                ? await query.CountAsync(condition, cancellationToken)
                : await query.CountAsync(cancellationToken);
        }

        public virtual TEntity Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            AttachIfNot(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;

            return entity;

        }

        public virtual Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            entity = Update(entity);

            return Task.FromResult(entity);
        }

        private void AttachIfNot(TEntity entity)
        {
            var entry = _dbContext.ChangeTracker.Entries().FirstOrDefault(x => x.Entity == entity);
            if (entry != null)
            {
                return;
            }

            dbSet.Attach(entity);
        }

        private IQueryable<TEntity> PrepareQuery(bool withDeleted = false, bool trackChanges = false)
        {
            var query = trackChanges
                ? dbSet.AsTracking()
                : dbSet.AsNoTracking();

            if (withDeleted)
            {
                query = query.IgnoreQueryFilters();
            }

            return query;
        }
    }
}
