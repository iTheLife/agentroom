﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Agentroom.Common.Interfaces;
using Agentroom.DataAccessLayer.Repository;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Agentroom.DataAccessLayer.Context
{
    public class EFUnitOfWork : IUnitOfWorkBase
    {
        protected readonly EfContext _dbContext;
        protected readonly ILogger _logger;

        public EFUnitOfWork(EfContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public EFUnitOfWork(EfContext dbContext, ILogger logger)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        
        public void Commit()
        {
            var changedEntriesCopt = _dbContext.ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added
                    || x.State == EntityState.Modified
                    || x.State == EntityState.Deleted)
                .ToList();
            _dbContext.SaveChanges();
        }

        public async Task<int> CommitAsync(CancellationToken cancellationToken = default)
        {
            return await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }

        public IRepositoryGeneric<TEntity> GetRepositoryEntity<TEntity>() where TEntity : class
        {
            return new EFGenericRepository<TEntity>(_dbContext);
        }

        public void RejectChanges()
        {
            foreach (var entry in _dbContext.ChangeTracker.Entries().Where(x=>x.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;

                    case EntityState.Modified:
                    case EntityState.Detached:
                        entry.Reload();
                        break;
                }
            }
        }
    }
}
