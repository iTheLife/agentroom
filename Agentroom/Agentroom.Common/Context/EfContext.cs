﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Agentroom.DataAccessLayer.EntityMapper;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Agentroom.DataAccessLayer.Context
{
    public class EfContext : DbContext
    {
        protected readonly ILogger _logger;

        public EfContext(DbContextOptions options)
            : base(options)
        {

        }

        public EfContext(DbContextOptions options, ILogger logger)
            : base(options)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            ApplyMappingConfigurationFromAssembly(modelBuilder, Assembly.GetExecutingAssembly());
        }

        protected void ApplyMappingConfigurationFromAssembly(ModelBuilder modelBuilder, Assembly assembly)
        {
            var entityMapperTypes = assembly.GetTypes()
                .Where(type =>
                    !string.IsNullOrEmpty(type.Namespace)
                    && type.BaseType?.IsGenericType == true
                    && type.BaseType.GetGenericTypeDefinition() == typeof(AbstractEntityMapper<>));

            foreach (var entityMapperType in entityMapperTypes)
            {
                dynamic instance = Activator.CreateInstance(entityMapperType);
                modelBuilder.ApplyConfiguration(instance);
            }
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}
